

<?php
// Parent class
abstract class Car {
  public $name;
  public $wheelsstatus;
  public $windshieldstatus;
  public $mirrorsstatus;
  public $headlightsstatus;
  public $steeringstatus;
  public $transmissiontype;
  public function __construct($name, $wheelsstatus, $windshieldstatus,$mirrorsstatus,$headlightsstatus, $steeringstatus, $transmissiontype) {
    $this->name = $name;
    $this->wheelsstatus = $wheelsstatus;
    $this->windshieldstatus = $windshieldstatus;
    $this->mirrorsstatus = $mirrorsstatus;
    $this->headlightsstatus = $headlightsstatus;
    $this->steeringstatus = $steeringstatus;
    $this->transmissiontype = $transmissiontype;
  }
  abstract public function attribute() : string;
  abstract public function wheelsstatus() : string;
  abstract public function windshieldstatus() : string;
  abstract public function mirrorsstatus() : string;
  abstract public function headlightsstatus() : string;
  abstract public function steeringstatus() : string;
  abstract public function transmissiontype() : string;




}

// Child classes
class Bmw extends Car {
  public function attribute() : string {
    return "Brand name: $this->name"; 
  }
  public function wheelsstatus() : string {
    return "Wheels Status: $this->wheelsstatus"; 
  }
  public function windshieldstatus() : string {
    return "Windshield status:  $this->windshieldstatus"; 
  }
  public function mirrorsstatus() : string {
    return "Mirrors istatusnfo: $this->mirrorsstatus"; 
  }
  public function headlightsstatus() : string {
    return "Headlight status: $this->headlightsstatus"; 
  }
  public function steeringstatus() : string {
    return "Steering status: $this->steeringstatus"; 
  }
  public function transmissiontype() : string {
    return "Transmission type: $this->transmissiontype"; 
  }


}



class Mercedes extends Car {
  public function attribute() : string {
    return "Brand name: $this->name"; 
  }
  public function wheelsstatus() : string {
    return "Wheels status: $this->wheelsstatus"; 
  }
  public function windshieldstatus() : string {
    return "Windshield status: $this->windshieldstatus"; 
  }
  public function mirrorsstatus() : string {
    return "Mirrors istatusnfo: $this->mirrorsstatus"; 
  }
  public function headlightsstatus() : string {
    return "Headlight status: $this->headlightsstatus"; 
  }
  public function steeringstatus() : string {
    return "Steering status: $this->steeringstatus"; 
  }
  public function transmissiontype() : string {
    return "Transmission type:
    
    
    $this->transmissiontype"; 
  }
}


// Create objects from the child classes
$bmw = new bmw("BMW","Yes","Yes","Yes","Yes","Yes","Auto");
echo $bmw->attribute(); echo "<br>";
echo $bmw->wheelsstatus(); echo "<br>";
echo $bmw->windshieldstatus(); echo "<br>";
echo $bmw->mirrorsstatus(); echo "<br>";
echo $bmw->headlightsstatus(); echo "<br>";
echo $bmw->steeringstatus(); echo "<br>";
echo $bmw->transmissiontype(); echo "<br>";
echo "<br><br><br><br>";

$mercedes = new mercedes("Mercedes","Yes","Yes","Yes","Yes","Yes","manual");
echo $mercedes->attribute();echo "<br>";
echo $bmw->wheelsstatus(); echo "<br>";
echo $bmw->windshieldstatus(); echo "<br>";
echo $bmw->mirrorsstatus(); echo "<br>";
echo $bmw->headlightsstatus(); echo "<br>";
echo $bmw->steeringstatus(); echo "<br>";
echo $bmw->transmissiontype(); echo "<br>";
echo "<br>";


?>




